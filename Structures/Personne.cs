﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Personnes.Structures
{
    class Personne : IDisposable
    {
        private string Nom { get; set; }
        private string Prenom { get; set; }
        private int Age { get; set; }
        static int compte = 0;

        public Personne() { }

        public Personne(string nom, string prenom, int age)
        {
            this.Nom = nom;
            this.Prenom = prenom;
            this.Age = age;
            compte++;
        }
        public void Afficher()
        {
            Console.WriteLine(this.Nom);
            Console.WriteLine(this.Prenom);
            Console.WriteLine(this.Age);
        }

        public void Combien()
        {
            Console.WriteLine(compte);
        }

        public void Dispose()
        {
            compte--;
        }
    }
}
